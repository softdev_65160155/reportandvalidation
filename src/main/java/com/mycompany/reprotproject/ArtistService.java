/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reprotproject;

import java.util.List;

/**
 *
 * @author Lenovo
 */
public class ArtistService {
    public List<ArtistReport> getToTenArtistByTotalPrice(){
    ArtistDao arttistDao = new ArtistDao();
    return arttistDao.getArtistByTotalPrice(10);
    }
    public List<ArtistReport> getToTenArtistByTotalPrice(String begin,String end){
    ArtistDao arttistDao = new ArtistDao();
    return arttistDao.getArtistByTotalPrice(begin,end,10);
    }
}
